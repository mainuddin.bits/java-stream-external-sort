package ag.pst.test.stream.sort;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Random;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import ag.pst.test.stream.sort.ItemStream.TestItem;

public class ItemStream implements Iterator<TestItem> {
	public static final int TEST_LIST_SIZE = 50000;

	// random number generator
	private static final Random RANDOM = new Random();

	private int count = 0;

	@Override
	public boolean hasNext() {
		return count < TEST_LIST_SIZE;
	}

	@Override
	public TestItem next() {
		// only use positive integers for easier debugging
		int key = Math.abs(RANDOM.nextInt());
		int number = Math.abs(RANDOM.nextInt());

		++count;

		return new TestItem(key + "", number);
	}

	public Stream<TestItem> getStream() {
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(this, Spliterator.ORDERED | Spliterator.CONCURRENT), false);
	}

	public static class TestItem implements Comparable<TestItem>, Serializable {
		private static final long serialVersionUID = -4158313799772890891L;

		private final String key;

		// long string to bloat up memory
		private final String superLongString;

		private final int number;

		public TestItem(String key, int number)
			{
				this.key = key;
				this.number = number;

				superLongString = key.repeat(10000);
			}

		public String getKey() {
			return key;
		}

		public String getSuperLongString() {
			return superLongString;
		}

		public int getNumber() {
			return number;
		}

		@Override
		public int compareTo(TestItem arg0) {
			return key.compareTo(arg0.key);
		}
	}
}
