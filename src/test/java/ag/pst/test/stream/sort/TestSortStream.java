package ag.pst.test.stream.sort;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import ag.pst.test.stream.sort.ItemStream.TestItem;

public class TestSortStream {
	// Uncomment the line below to re-enable this test
	// @Test
	void testSortByStoringInMemory() throws Exception {
		Stream<TestItem> unsortedStream = new ItemStream().getStream();

		SortStream<TestItem> sorter = new SortStream<>();

		System.out.println("Sorting stream");

		Stream<TestItem> sortedStream = sorter.sortByStoringInMemory(unsortedStream);

		evaluateSortedStream(sortedStream);
	}

	@Test
	void testSortWithoutStoringInMemory() throws Exception {
		Stream<TestItem> unsortedStream = new ItemStream().getStream();

		SortStream<TestItem> sorter = new SortStream<>();

		System.out.println("Sorting stream");

		Stream<TestItem> sortedStream = sorter.sortWithoutStoringInMemory(unsortedStream, TestItem::getKey);

		evaluateSortedStream(sortedStream);
	}

	private void evaluateSortedStream(Stream<TestItem> sortedStream) {
		System.out.println("Evaluating sorted stream");

		// wrapper to contain the previous item
		ItemWrapper wrapper = new ItemWrapper();

		// start processing the sorted stream
		sortedStream.forEach(item -> {
			// get previous item
			TestItem previousItem = wrapper.item;

			if (previousItem != null) {
				String previousKey = previousItem.getKey();
				String currentKey = item.getKey();

				// compare previous to current item
				int comparisonResult = previousKey.compareTo(currentKey);

				// previous must be less or equal than current according to our key comparison
				assertTrue(comparisonResult <= 0,
					"Stream is not correctly sorted; previous: " + previousKey + ", current: " + currentKey);
			}

			// set previous = current
			wrapper.setItem(item);

			// sleep for 5 seconds at the last item so we have time to check memory consumption
			if (wrapper.count == ItemStream.TEST_LIST_SIZE) {
				System.out.println("On last item");

				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		assertNotNull(wrapper.item, "Stream is empty");
	}

	private static class ItemWrapper {
		private TestItem item;

		private int count = 0;

		private void setItem(TestItem item) {
			this.item = item;

			++count;
		}
	}
}
